package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResult;
    private Button btnSumar, btnRestar, btnMulti, btnDiv, btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniComponents();
    }

    public void iniComponents(){
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        txtResult = findViewById(R.id.txtResult);

        btnSumar = findViewById(R.id.btnSuma);
        btnRestar = findViewById(R.id.btnResta);
        btnMulti = findViewById(R.id.btnMult);

        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);

        setEventos();
    }
    public void setEventos(){
        this.btnCerrar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnMulti.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnSumar.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

    }
}
